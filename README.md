# redis-pokemon

Test Redis JSON operations

## Packages Installation

Install package dependencies

```shell
pipenv --python 3.10
pipenv shell
pipenv install --dev
```

## Provide environ variables file

Use example .env file

```shell
cp .env.example .env
```

## License

[MIT](LICENSE)
