import os
from app.init import logger
from app.constants import LOG_DISPLAY_ENV_VARS, REDIS_HOST, REDIS_PORT, REDIS_PASSWORD
from app.pokemon import get_pokemons, get_pokemons_details
from app.redis_ops import store_pokemon_details, hash_store, clear_redis_data, create_redis_index, store_pokemons, get_data, store_pokemons_items, store_pokemons_array_items
import redis

from fastapi import FastAPI
app = FastAPI()

r = redis.Redis(host=REDIS_HOST, port=int(
    REDIS_PORT), password=REDIS_PASSWORD)


def main(display_env: str = LOG_DISPLAY_ENV_VARS) -> None:
    """ Main Process """

    if display_env == "True":
        logger.debug("## Environment variables")
        logger.debug(os.environ)

    clear_redis_data()
    create_redis_index()

    pokemons = get_pokemons()
    if pokemons:
        store_pokemons(pokemons)
        # store_pokemons_items(pokemons) # testing
        # store_pokemons_array_items(pokemons) #testing
    else:
        raise Exception("pokemons empty, not storing!")

    details_list = []

    for pokemon in pokemons["results"]:
        logger.debug(pokemon["url"])
        details = get_pokemons_details(pokemon["url"])
        logger.debug(details)
        if details:
            details_list.append({pokemon["name"]: details})
        else:
            raise Exception("details empty, not added to list!")
    logger.debug(details_list)

    if details_list:
        store_pokemon_details({"pokemons_details": details_list})
    else:
        raise Exception("details_list empty, not storing!")

    logger.info(get_data("pokemons"))
    logger.debug(get_data("pokemons_details"))


if __name__ == "__main__":
    hash_store()


@app.get("/")
def read_root():
    """ Get pokemons """
    data = get_data("pokemons")
    return data


@app.get("/{name}")
def get_pokemon_url(name: str):
    """ Get pokemon url """

    data = r.json().get(
        "pokemons", f"$.pokemons[?(@.name == '{name}')]")
    details = r.json().get("pokemons_details",
                           f"$.pokemons_details[?(@.pokemon.name == '{name}')]")

    return {data: {"details": details}}


@app.get("/details/{name}")
def get_pokemon_details(name: str):
    """ Get pokemon details """

    data = r.ft("pokemonsIdx").search(
        f"@name:({name})")

    return data
