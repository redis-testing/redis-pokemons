from app.init import logger
from app.constants import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD
from app.pokemon import get_pokemons, get_pokemons_details
import redis
from redis.commands.json.path import Path
from redis.commands.search.indexDefinition import IndexDefinition, IndexType
from redis.commands.search.field import TextField

r = redis.Redis(host=REDIS_HOST, port=int(REDIS_PORT), password=REDIS_PASSWORD)
logger.debug(r)


def get_data(key: str) -> list:
    """ Get data from Redis DB """

    try:
        data = r.json().get(key)
        logger.debug(data)
    except Exception as e:
        logger.error(e)
        raise

    return data


def store_pokemon_details(details: dict) -> bool:
    try:
        r.json().set("pokemons_details", Path.root_path(), details)
    except Exception as e:
        logger.error(e)
        raise

    return True


def clear_redis_data() -> bool:
    try:
        r.flushall()
    except Exception as e:
        logger.error(e)
        raise

    return True


def create_redis_index() -> bool:
    """ Create Redis index """

    name_index_definition = IndexDefinition(
        index_type=IndexType.JSON, prefix=["pokemons"])
    name_index_schema = (
        TextField('$.pokemons[*].name', as_name='name'),
        TextField('$.pokemons[*].url', as_name='url'),
    )

    try:
        r.ft("pokemonsIdx").create_index(
            name_index_schema, definition=name_index_definition)
    except Exception as e:
        logger.error(e)
        raise

    return True


def hash_store() -> bool:
    """ Hash store pokemons into Redis DB """

    clear_redis_data()

    api_data = get_pokemons()
    i = 0
    for pokemon in api_data["results"]:
        i = i+1
        r.hset(name=f"pokemon:{i}", mapping={
               "name": pokemon["name"], "url": pokemon["url"]})
        # print(r.hgetall(f"pokemon:{i}"))

    return True


def store_pokemons_items(pokemons: dict) -> bool:
    try:
        [r.json().set(pokemon["name"], Path.root_path(), pokemon)
         for pokemon in pokemons["results"]]
    except Exception as e:
        logger.error(e)
        raise

    return True


def store_pokemons_array_items(pokemons: dict) -> bool:
    """ Store individual json objects into Redis DB as JSON documents """

    try:
        r.json().set("pokemons", Path.root_path(), [])
        [r.json().arrappend("pokemons", Path.root_path(), pokemon)
         for pokemon in pokemons["results"]]
    except Exception as e:
        logger.error(e)

    return True


def store_pokemons(pokemons: dict) -> bool:
    """ Store json objects into Redis DB """

    try:
        store = r.json().set("pokemons", Path.root_path(),
                             {"pokemons": pokemons["results"]}
                             )
        logger.debug(store)
    except Exception as e:
        logger.error(e)
        raise

    return True
