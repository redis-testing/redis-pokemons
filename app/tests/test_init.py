from app.constants import (
    LOG_PATH,
    LOG_CLEAR,
    LOG_LEVEL,
)


def test_init():
    assert LOG_PATH != ""
    assert LOG_CLEAR != ""
    assert LOG_LEVEL != ""
