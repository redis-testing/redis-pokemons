from app.pokemon import get_pokemons, get_pokemons_details
from app.constants import API_URL, API_QUERY_LIMIT
from app.pokemon import Pokemon
from typing import List


def test_api_url(api_url: str = API_URL) -> None:
    assert api_url != ""
    assert api_url.find("http") != -1


def test_get_pokemons(api_url: str = API_URL, api_query_limit: str = API_QUERY_LIMIT) -> None:
    pokemons = get_pokemons(api_url, api_query_limit)
    assert isinstance(pokemons, dict)
    # assert isinstance(pokemons, Pokemon)


def test_get_pokemons_details() -> None:
    assert get_pokemons_details("https://pokeapi.co/api/v2/ability/7/") != None
