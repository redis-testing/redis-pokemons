from dataclasses import dataclass
from typing import List
from app.init import logger
from app.constants import API_URL, API_QUERY_LIMIT
import requests


@dataclass
class Ability:
    name: str
    url: str


@dataclass
class Pokemon:
    name: str
    url: str
    abilities: List[Ability]


def get_pokemons(api_url: str = API_URL, api_query_limit: str = API_QUERY_LIMIT) -> dict:
    """ Get data from remote API """

    if api_url != "" and api_query_limit != "":
        url = f"{api_url}&limit={api_query_limit}"
        pokemons = requests.get(url)
        logger.debug(pokemons)
    else:
        raise Exception("API param(s) missing!")

    return pokemons.json()


def get_pokemons_details(url: str) -> dict:
    """Get pokemons details from url"""

    if url != "":
        id = url.split("/")[-2]
        details_url = f"https://pokeapi.co/api/v2/ability/{id}/"
        logger.debug(details_url)
        api_data = requests.get(details_url)
        logger.debug(api_data)
    else:
        raise Exception("url arg missing!")

    return api_data.json()
