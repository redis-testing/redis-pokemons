import os
from app.logger import init_logger
from app.constants import (
    LOG_PATH,
    LOG_CLEAR,
    LOG_LEVEL,
)

if not os.path.exists(LOG_PATH):
    os.mkdir(LOG_PATH)

log_files = (
    f"{os.path.abspath}../../{LOG_PATH}/app.log",
    f"{os.path.abspath}../../{LOG_PATH}/app.warning.log",
    f"{os.path.abspath}../../{LOG_PATH}/app.error.log",
)

if LOG_LEVEL == "DEBUG":
    testing_mode = True
else:
    testing_mode = False

logger = init_logger(__name__, testing_mode=testing_mode)

if LOG_CLEAR == "True":
    for log_file in log_files:
        if os.path.exists(log_file):
            with open(log_file, "r+") as t:
                t.truncate(0)
