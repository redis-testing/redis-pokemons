import os
from dotenv import load_dotenv

ROOT_DIR = os.path.dirname(os.path.abspath(".env"))
load_dotenv(dotenv_path=f"{ROOT_DIR}/.env", override=True)

LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO").upper()
LOG_PATH = os.getenv("LOG_PATH", "../logs")
LOG_CLEAR = os.getenv("LOG_CLEAR", "False").capitalize()
LOG_DISPLAY_ENV_VARS = os.getenv("LOG_DISPLAY_ENV_VARS", "False").capitalize()
API_URL = os.getenv("API_URL", "")
API_QUERY_LIMIT = os.getenv("API_QUERY_LIMIT", "10")
REDIS_HOST = os.getenv("REDIS_HOST", "")
REDIS_PORT = os.getenv("REDIS_PORT", "13144")
REDIS_PASSWORD = os.getenv("REDIS_PASSWORD", "")
